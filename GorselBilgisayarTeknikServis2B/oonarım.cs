﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBilgisayarTeknikServis2B
{
    public partial class oonarım : Form
    {
          Müşteri v;
        public oonarım (Müşteri l)
        {
            InitializeComponent();
            v = l;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            bilgisayartakipEntities4 ulas = new bilgisayartakipEntities4();
            List<Onarım> liste = ulas.Onarım.Where(a => a.Adi.Contains(textBox1.Text)).ToList();
            dataGridView1.DataSource = liste;
        }

        private void oonarım_Load(object sender, EventArgs e)
        {

            textBox1.Text = v.ID.ToString();
            textBox2.Text = v.Adi;
        }
    }
}
