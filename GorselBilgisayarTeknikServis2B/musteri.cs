﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselBilgisayarTeknikServis2B
{
    public partial class musteri : Form
    {
        public musteri()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form1 c = new Form1();
            this.Hide();
            c.ShowDialog();

        }

        private void button1_Click(object sender, EventArgs e)
        {

            SqlConnection baglan = new SqlConnection(@"Data source=YASINBEYLER;initial catalog=bilgisayartakip; integrated security=true");
            SqlCommand cmd = new SqlCommand("insert into Müşteri(Adi,Soyadi,Tel)values(@Adi,@Soyadi,@Tel)", baglan);
            baglan.Open();
            cmd.Parameters.AddWithValue("@Adi", textBox3.Text);
            cmd.Parameters.AddWithValue("@Soyadi", textBox2.Text);
            cmd.Parameters.AddWithValue("@Tel", textBox1.Text);
            cmd.ExecuteNonQuery();
            baglan.Close();
            MessageBox.Show("Başarıyla Eklendi", "Bilgi");
            bilgisayartakipEntities4 ulas = new bilgisayartakipEntities4();
            List<Müşteri> liste = ulas.Müşteri.ToList();
            dataGridView1.DataSource = liste;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Müsterigüncelle x = new Müsterigüncelle();
            this.Hide();
            x.ShowDialog();
        }

        private void musteri_Load(object sender, EventArgs e)
        {

          
            bilgisayartakipEntities4 ulas = new bilgisayartakipEntities4();
            List<Müşteri> liste = ulas.Müşteri.ToList();
            dataGridView1.DataSource = liste;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bilgisayartakipEntities4 ulas = new bilgisayartakipEntities4();
            List<Müşteri> liste = ulas.Müşteri.Where(a => a.Adi.Contains(textBox4.Text)).ToList();
            dataGridView1.DataSource = liste;
            

        }

        private void button6_Click(object sender, EventArgs e)
        {
            bilgisayartakipEntities4 ulas = new bilgisayartakipEntities4();
            List<Müşteri> liste = ulas.Müşteri.ToList();
            dataGridView1.DataSource = liste;
        }
    }
}
